import { Outlet } from 'react-router-dom';

import {
  useFlag,
  useFlagsStatus,
  useUnleashContext,
} from '@unleash/proxy-client-react';

import ReactGA from 'react-ga4';

import { MyContext } from './MyContext';
import ReloadPrompt from './ReloadPrompt';
import './App.css';
import { useContext, useEffect } from 'react';

function App() {
  // replaced dyanmicaly
  const date = '__DATE__';
  const { flagsReady, flagsError } = useFlagsStatus();
  const updateContext = useUnleashContext();
  const x = useContext(MyContext);

  const enabled = useFlag('test01');

  // << This component itself seems not refreshed when the flag becomes ready >>
  //
  // if (!flagsReady) {
  //   console.log('unleash', flagsReady, FlagContext, flagsError);
  // }
  //

  useEffect(() => {
    const initGA = (userId: string) => {
      ReactGA.initialize('G-LEYNG7RVY7', {
        gaOptions: {
          userId,
        },
      });
      ReactGA.send('pageview');
    }

    if (!x?.user?.address) {
      initGA("n/a")
      return;
    }

    updateContext({ userId: x.user.address }).then(() => {
      console.log('Unleash context updated: userId');
    });
    initGA(x.user.address);

  }, [x?.user?.address]);

  return (
    <main className="App">
      <img src="/favicon.svg" alt="PWA Logo" width="60" height="60" />
      <h1 className="Home-title">TwoShots</h1>
      <div className="Home-built">
        Built at:
        {date}
      </div>
      <div style={{ borderStyle: 'solid', borderColor: 'coral' }} className="Home-unleadh">
        {flagsReady ? 'Ready' : 'NotReady'} = {enabled ? 'true' : 'false'}
        <br />
        {flagsError ? `${flagsError}` : 'No err'}
      </div>
      <Outlet />
      <ReloadPrompt />
    </main>
  );
}

export default App;
