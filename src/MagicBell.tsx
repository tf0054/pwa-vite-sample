// import {
//   getAuthToken,
//   registerServiceWorker,
//   isSubscribed,
//   subscribe,
//   prefetchConfig,
// } from '@magicbell/webpush';
import MagicBell, { FloatingNotificationInbox, useNotifications } from '@magicbell/magicbell-react';

// import { useEffect, useRef, useState } from 'react';

const apiKey = import.meta.env.VITE_MAGICBELL_KEY;

// export const MagicBellInitial = ({
//   email,
//   extId,
// }: {
//   email: string | undefined;
//   extId: string;
// }) => {
//   const calledOnce = useRef(true);
//   const [msg, setMsg] = useState('');
//   const notifications = useNotifications();

//   if (!extId) return <></>;

//   useEffect(() => {
//     if (!calledOnce.current) return;
//     calledOnce.current = false;

//     const asyncFunc = async () => {
//       console.log("notifications", notifications)
//       const resAuth = await getAuthToken({
//         apiKey: apiKey,
//         userEmail: email,
//         userExternalId: extId,
//       });
//       console.log('mb-auth', resAuth);
//       setMsg('authed');
//       //
//       const serviceWorkerPath = '/push/magicbell/sw.js';
//       //
//       const pswk = registerServiceWorker({
//         path: serviceWorkerPath,
//       });
//       const pcfg = prefetchConfig({
//         token: resAuth.token,
//         project: resAuth.project,
//         serviceWorkerPath,
//       });
//       await Promise.all([pswk, pcfg]).then(() => {
//         setMsg('prefetched');
//       });
//       //
//       const subscribed = await isSubscribed({
//         token: resAuth.token,
//         project: resAuth.project,
//         serviceWorkerPath: '/push/magicbell/sw.js',
//       });
//       if (!subscribed) {
//         await subscribe({
//           token: resAuth.token,
//           project: resAuth.project,
//           serviceWorkerPath,
//         });
//         setMsg('subscribed');
//       } else {
//         setMsg('already subscribed');
//       }
//     };
//     setMsg('initializing mb');
//     asyncFunc();
//   }, []);

//   return (
//     <>
//       <MagicBell apiKey={apiKey} userExternalId={extId}>
//         {(props) => <FloatingNotificationInbox height={300} {...props} />}
//       </MagicBell>
//       <div style={{ borderStyle: 'solid', borderColor: 'blue' }}>
//         {msg}
//         <br /># of notifications: {notifications?.total}
//       </div>
//     </>
//   );
// };

export const MagicBellReuse =  ({
  email,
  extId,
}: {
  email: string | undefined;
  extId: string;
}) => {
  console.log("MagicBellReuse", extId)
  return (
    <MagicBell apiKey={apiKey} userEmail={email} userExternalId={extId}>
      {(props) => <FloatingNotificationInbox height={300} {...props} />}
    </MagicBell>
  );
};
