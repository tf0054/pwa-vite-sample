import { Dispatch, createContext } from 'react';

export type userType = { address: string; email: string; token?: string };

export type userContextType =
  | {
      user: undefined | userType;
      setUser: undefined | Dispatch<any>;
    }
  | undefined;

export const MyContext = createContext<userContextType>({
  user: undefined,
  setUser: undefined,
});
