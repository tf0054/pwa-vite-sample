import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { createRoot } from 'react-dom/client';

import { FlagProvider } from '@unleash/proxy-client-react';

import './index.css';
import App from './App';
import Home from './pages/Home';
import About from './pages/About';
import Hi from './pages/hi/[name]';
import { MyContext, userType } from './MyContext';
import { localCache, magicDidtokenValidate } from './util';

const config = {
  url: 'https://ueproxy-tf0054.koyeb.app/proxy', // Your front-end API URL or the Unleash proxy's URL (https://<proxy-url>/proxy)
  clientKey: 'totally_secret', // A client-side API token OR one of your proxy's designated client keys (previously known as proxy secrets)
  refreshInterval: 5, // How often (in seconds) the client should poll the proxy for updates
  appName: 'pwa-vite', // The name of your application. It's only used for identifying your application
};

const Main = () => {
  const [user, setUser] = useState<userType>();

  useEffect(() => {
    const didToken: any | null = localCache.getItem('didToken');

    if (didToken) {
      console.log("didToken found", didToken)
      magicDidtokenValidate(didToken).then(async (res) => {
        const resJson = await res.json();
        if ("authenticated" in resJson) {
          console.log("decoded", resJson);
          setUser({ email: resJson.email, address: resJson.address });
        } else {
          console.error("didToken seems not-set or expired");
        }
      })
    }
  }, []);
  
  return (
    <React.StrictMode>
      <FlagProvider config={{ ...config, context: { userId: '' } }}>
        <MyContext.Provider value={{ user, setUser }}>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<App />}>
                <Route index element={<Home />} />
                <Route path="/about" element={<About />} />
                <Route path="/hi">
                  <Route path=":name" element={<Hi />} />
                </Route>
              </Route>
            </Routes>
          </BrowserRouter>
        </MyContext.Provider>
      </FlagProvider>
    </React.StrictMode>
  );
};

createRoot(document.getElementById('app')!).render(<Main />);
