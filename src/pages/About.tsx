import { useContext, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router';
import ReactGA from 'react-ga4';

import { MyContext } from '../MyContext';

function About() {
  const calledOnce = useRef(true);

  // replaced dyanmicaly
  const date = '__DATE__';

  const router = useNavigate();
  const x = useContext(MyContext);

  useEffect(() => {
    if (!calledOnce.current) return;
    calledOnce.current = false;

    ReactGA.send({ hitType: "pageview", page: "/about", title: "About page" });
  }, []);

  return (
    <div className="About">
      <div>
        <strong>/about</strong> route, built at: {date}
      </div>
      {x?.user?.address}
      <br />
      <a
        onClick={() => {
          router(`/`);
        }}
        style={{
          color: 'blue',
          textDecorationLine: 'underline',
        }}
      >
        Go Home
      </a>
      <a
        onClick={() => {
          router(`https://card-aid.com/o`);
        }}
        style={{
          color: 'blue',
          textDecorationLine: 'underline',
        }}
      >
        Go CARD AID
      </a>
    </div>
  );
}

export default About;
