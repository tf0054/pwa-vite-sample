import { useContext, useEffect, useRef, useState } from 'react';
import { redirect, useNavigate } from 'react-router';

import { LoginWithMagicLinkConfiguration, Magic } from 'magic-sdk';
import { AuthExtension } from '@magic-ext/auth';
import { InstanceWithExtensions, SDKBase } from '@magic-sdk/provider';

import ReactGA from 'react-ga4';

import './Home.css';
import { MagicBellReuse } from '../MagicBell';
import { WebPush } from '../WebPush';
import { localCache, reduceAddress } from '../util';
import { MyContext } from '../MyContext';
import { useFlag } from '@unleash/proxy-client-react';

function Home() {
  const [count, setCount] = useState(0);
  const calledOnce = useRef(true);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [msg, setMsg] = useState('');
  const [MBell, setMBell] = useState(<></>);
  const [magicObj, setMagicObj] = useState<InstanceWithExtensions<SDKBase, AuthExtension[]>>();

  const router = useNavigate();

  const x = useContext(MyContext);
  const xFound = x?.user?.address ? true : false;

  const enabled = useFlag('about');

  const setAddrEmailFromMagic = async () => {
    let data: undefined | { email: string; address: string };
    return magicObj?.user
      .getInfo()
      .then((userMetadata) => {
        console.log('userMetadata', userMetadata);
        if (userMetadata) {
          if (userMetadata.publicAddress && userMetadata.email) {
            data = {
              email: userMetadata.email,
              address: userMetadata.issuer!.substring(9),
            };
            //
            if (x?.setUser !== undefined) {
              x?.setUser(data);
              return data;
            } else console.log('No setUser?');
          } else {
            setMsg('Wrong userMetadata');
            console.log('Wrong userMetadata', userMetadata);
          }
        } else {
          setMsg('userMeta is empty');
        }
        return data;
      })
      .catch((e: any) => {
        setMsg('Please login with Magic (Cannot get userMetadata)');
        console.error(e);
        return data;
      });
  };

  // @ts-expect-error just ignore
  const handleChange = (event) => {
    setName(event.target.value || '');
  };
  const handleEmailChange = (event: any) => {
    setEmail(event.target.value || '');
  };
  // @ts-expect-error just ignore
  const handleSubmit = (event) => {
    event.preventDefault();
    if (name) router(`/hi/${name}`);
  };

  const handleEmailSubmit = (event: any) => {
    event.preventDefault();
    const asyncFunc = async () => {
      const didToken = await magicObj?.auth.loginWithEmailOTP({
        email: email,
        showUI: true,
      } as LoginWithMagicLinkConfiguration);
      console.log('didToken', reduceAddress(didToken ? didToken : ""));

      const ownDidToken = await magicObj?.user.getIdToken({ lifespan: 60 * 60 * 24 * 7 });
      console.log('ownDidToken', ownDidToken);
      localCache.setItem("didToken", ownDidToken, 60*24*7);

      setAddrEmailFromMagic();
    };
    asyncFunc();
  };

  const logout = () => {
    const m = new Magic(import.meta.env.VITE_MAGIC_AUTH_KEY, {
      extensions: [new AuthExtension()],
      network: {
        rpcUrl: import.meta.env.VITE_RPC_URL,
        chainId: import.meta.env.VITE_RPC_CHAIN_ID,
      },
    });
    m.preload().then(async () => {
      console.log('preloaded/logout');
      m.user.logout().then(() => {
        localCache.setItem("didToken", "");
        window.location.href =
          window.location.protocol +
          '//' +
          window.location.hostname +
          ':' +
          window.location.port +
          '/';
      });
    });
  };

  useEffect(() => {
    if (!calledOnce.current) return;
    calledOnce.current = false;

    if (xFound) {
      console.log('Magic isnt needed to be initialized', x?.user?.address, x?.user?.email);
      // setAddress(x.user.address);
      // setEmail(x.user.email);
    } else {
      console.log('Magic is being initialized');
      const m = new Magic(import.meta.env.VITE_MAGIC_AUTH_KEY, {
        extensions: [new AuthExtension()],
        network: {
          rpcUrl: import.meta.env.VITE_RPC_URL,
          chainId: import.meta.env.VITE_RPC_CHAIN_ID,
        },
      });
      m.preload().then(async () => {
        console.log('preloaded');
        setMagicObj(m);
      });
    }
  }, []);

  useEffect(() => {
    //@ts-ignore
    if (xFound)
      setMBell(
        <>
          zzz
          <MagicBellReuse email={x?.user?.email} extId={x?.user?.address} />
        </>
      );
    else if (magicObj)
      setAddrEmailFromMagic().then((data) => {
        console.log('xxxx');
        if (data)
          setMBell(
            <>
              xxxx
              <WebPush email={data.email} extId={data.address} />
              <MagicBellReuse email={data.email} extId={data.address} />
            </>
          );
        else {
          setMBell(<>Cannot setMBell</>);
        }
      });
    else console.log('Waiting magic');
  }, [magicObj, x]);

  if (!xFound && !magicObj) return <div>Waiting to get address</div>;
  else
    return (
      <div className="Home">
        {x?.user?.address && (
          <>
            {MBell}
            <p>
              {reduceAddress(x?.user?.address)}, {x?.user?.email}
            </p>
            <p>
              <button type="button" onClick={() => setCount((count) => count + 1)}>
                count is: {count}
              </button>
            </p>
            <br />
          </>
        )}
        {x?.user?.address ? (
          <>
            <form onSubmit={handleSubmit}>
              <input
                value={name}
                onChange={handleChange}
                type="text"
                aria-label="What's your name?"
                placeholder="What's your name?"
              />
              <button type="submit">GO</button>
            </form>
            <span onClick={logout}>logout</span>
          </>
        ) : (
          <div style={{ borderStyle: 'solid', borderColor: 'yellow' }}>
            {msg}
            <form onSubmit={handleEmailSubmit}>
              <input
                value={x?.user?.email}
                onChange={handleEmailChange}
                type="text"
                aria-label="What's your email?"
                placeholder="What's your email?"
              />
              <button type="submit">SET</button>
            </form>
          </div>
        )}
        {enabled && (
          <>
            <br />
            <a
              onClick={() => {
                ReactGA.event({
                  category: 'your category',
                  action: 'ReactGA4 action',
                  label: 'your label', // optional
                  value: 99, // optional, must be a number
                  // nonInteraction: true, // optional, true/false
                  transport: 'xhr', // optional, beacon/xhr/image
                });
                router(`/about`);
              }}
              style={{
                color: 'blue',
                textDecorationLine: 'underline',
              }}
            >
              About
            </a>
            <br/>
          </>
        )}
        <br />
        <br />
            <a
              onClick={() => {
                ReactGA.event({
                  category: 'your category',
                  action: 'ReactGA4 action',
                  label: 'your label', // optional
                  value: 99, // optional, must be a number
                  // nonInteraction: true, // optional, true/false
                  transport: 'xhr', // optional, beacon/xhr/image
                });
                window.location.href = `https://wapoap01.poap.dev/o`;
              }}
              style={{
                color: 'blue',
                textDecorationLine: 'underline',
              }}
            >
              External
            </a>
            <br/>
        {magicObj &&
              <a onClick={async () => {

                await magicObj.wallet.showUI();
              }}>magic wallet</a>}
        {/* { import.meta.env.VITE_RPC_URL} */}
      </div>
    );
}

export default Home;
