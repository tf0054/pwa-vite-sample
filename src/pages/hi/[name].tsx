import { useNavigate, useParams } from 'react-router';

import ReactGA from 'react-ga4';

function Hi() {
  // replaced dyanmicaly
  const date = '__DATE__';
  const params = useParams();
  const router = useNavigate();

  ReactGA.send({ hitType: "pageview", page: `/hi/${params.name}`, title: `Hi ${params.name}` });

  return (
    <div>
      <div>
        <strong>/hi</strong> route, built at: {date}
      </div>
      <p>
        Hi:
        {params.name}
      </p>
      <br />
      <a
        onClick={() => {
          router(`/`);
        }}
        style={{
          color: 'blue',
          textDecorationLine: 'underline',
        }}
      >
        Go Home
      </a>
    </div>
  );
}

export default Hi;
