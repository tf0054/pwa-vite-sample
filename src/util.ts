import browserLocalstorage from 'browser-localstorage-expire';

export const localCache = browserLocalstorage();

export const reduceAddress = (address: string) => {
  if (address.length < 10) return address;
  return address.slice(0, 6) + '\u2026' + address.slice(-4);
};

export async function magicDidtokenValidate(didToken: string) {
  return fetch(`${import.meta.env.VITE_API_ROOT}/inc/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + didToken,
    },
    body: JSON.stringify({email: "hi@poap.dev"}),
  });
}